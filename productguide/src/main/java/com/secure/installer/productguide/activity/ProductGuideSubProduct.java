package com.secure.installer.productguide.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.secure.installer.model.ProductTableData;
import com.secure.installer.productguide.R;
import com.secure.installer.productguide.adapter.ProductGuideSubRecyclerViewAdapter;
import com.secure.installer.productguide.interfaces.ItemClickListener;
import com.secure.installer.securedatabase.AppDatabase;
import com.secure.installer.securedatabase.AppExecutors;
import com.secure.installer.securedatabase.GlobalClass;

import java.util.ArrayList;
import java.util.List;

public class ProductGuideSubProduct extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView header;
    List<ProductTableData> product_list = new ArrayList<>();
    ProductGuideSubRecyclerViewAdapter adapter;

    AppDatabase mDb;
    String productCategory;
    String productSubCategory;
    RelativeLayout nodataLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_guide_sub_product_layout);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        header = findViewById(R.id.header);
        recyclerView = findViewById(R.id.recyclerView);
        nodataLayout = findViewById(R.id.nodataLayout);

        // setup toolbar
        Toolbar mToolbar = findViewById(R.id.toolbar);
        GlobalClass.setToolbar(this, mToolbar, getString(R.string.product_guide));

        productCategory = getIntent().getStringExtra("product_category");
        productSubCategory = getIntent().getStringExtra("name");

        header.setText("Select " + productSubCategory);
        mDb = AppDatabase.getInstance(ProductGuideSubProduct.this);



        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setUpData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.home) {
            GlobalClass.navigateToHomePage(ProductGuideSubProduct.this);
        }
        return super.onOptionsItemSelected(item);
    }


    void setUpData() {

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                try {

                    if (productSubCategory.toLowerCase().equalsIgnoreCase("TimeSwitches & programmers")) {

                        List<ProductTableData> programmer_list = mDb.productDetailsDao().getSubCategoryProduct(productCategory, "programmer");
                        List<ProductTableData> timeswitch_list = mDb.productDetailsDao().getSubCategoryProduct(productCategory, "timeswitch");

                        product_list.addAll(programmer_list);
                        product_list.addAll(timeswitch_list);

                    } else {
                        product_list = mDb.productDetailsDao().getSubCategoryProduct(productCategory, productSubCategory.toLowerCase());
                    }



                    if (product_list.size() == 0) {
                        nodataLayout.setVisibility(View.VISIBLE);
                    } else {
                        nodataLayout.setVisibility(View.GONE);
                    }


                    adapter = new ProductGuideSubRecyclerViewAdapter(ProductGuideSubProduct.this, product_list, new ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            GlobalClass.navigateToWebView(ProductGuideSubProduct.this);

                        }
                    });
                    recyclerView.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }
}
