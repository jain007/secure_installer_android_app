package com.secure.installer.productguide.interfaces;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View view, int position);
}
