package com.secure.installer;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import com.secure.installer.installnewproduct.activity.InstallNewProduct;
import com.secure.installer.productguide.activity.ProductGuide;
import com.secure.installer.replaceprogremmer.activity.ReplaceProgrammerManufacturerNameList;
import com.secure.installer.securedatabase.SecureDatabase;
import com.secure.installer.startcommissioning.StartCommissioning;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DrawerLayout mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.secure_controls);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.baseline_menu);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                  mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        TextView add_remove_device = findViewById(R.id.add_remove_device);
        TextView drawerIconClose = findViewById(R.id.drawerIconClose);
        TextView replaceProgram = findViewById(R.id.replaceProgram);
        TextView installProduct = findViewById(R.id.installProduct);
        TextView product_guide = findViewById(R.id.product_guide);
        TextView quick_pair = findViewById(R.id.quick_pair);

        drawerIconClose.setFilterTouchesWhenObscured(true);
        replaceProgram.setFilterTouchesWhenObscured(true);
        installProduct.setFilterTouchesWhenObscured(true);
        product_guide.setFilterTouchesWhenObscured(true);
        quick_pair.setFilterTouchesWhenObscured(true);
        add_remove_device.setFilterTouchesWhenObscured(true);

        SecureDatabase database = new SecureDatabase(MainActivity.this, MainActivity.this);
        database.getDataBaSe(MainActivity.this);

        drawerIconClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
        });

        replaceProgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ReplaceProgrammerManufacturerNameList.class));
            }
        });

        installProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), InstallNewProduct.class));
            }
        });

        product_guide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ProductGuide.class));
            }
        });

        quick_pair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), StartCommissioning.class));
            }
        });

        add_remove_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), StartCommissioning.class));
            }
        });

    }
}