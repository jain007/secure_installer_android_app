package com.secure.installer.securedatabase;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.secure.installer.model.DataBaseModelResponse;
import com.secure.installer.model.DataBaseResponse;
import com.secure.installer.model.Helper;
import com.secure.installer.model.ProductTableData;

import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecureDatabase {

    static Activity act;
    static Context ctx;

    private static ProgressDialog progressDialog;
    private static ProgressDialog progressDialog2;

    private static AppDatabase mDb;

    private static RequestQueue mRequestQueue;
    public static final Object LOCK = new Object();

    public SecureDatabase(Context ctx, Activity act) {
        this.ctx = ctx;
        this.act = act;
    }

    public static void getDataBaSe(Context context) {

        mDb = AppDatabase.getInstance(context);

        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFD4D9D0")));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);

        progressDialog2 = new ProgressDialog(context);
        progressDialog2.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog2.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFD4D9D0")));
        progressDialog2.setCancelable(false);
        progressDialog2.setIndeterminate(false);

        insertDataToDatabase(context);

    }


    private static String loadJSONFromAsset(Context context) {
        try {
            InputStream is = context.getAssets().open("databasefile.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            return json;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }

    }

    private static void insertDataToDatabase(Context context) {

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {

                try {

                    int size = mDb.productDetailsDao().getCount();

                    if (size == 0) {

                        showDialog("Please wait...");

                        JSONObject obj = new JSONObject(loadJSONFromAsset(context));

                        if (obj != null) {
                            JSONArray jsonArray = obj.getJSONArray("responseObj");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject json_inside = jsonArray.getJSONObject(i);

                                ProductTableData product = new ProductTableData();

                                product.setId(json_inside.getInt("id"));
                                product.setManufacturerName(json_inside.getString("manufacturerName"));
                                product.setProductModelName(json_inside.getString("productModelName"));
                                product.setOtherCompatibleProductName(json_inside.getString("otherCompatibleProductName"));
                                product.setRecommendProductName(json_inside.getString("recommendProductName"));
                                product.setPair1(json_inside.getString("pair1"));
                                product.setPair2(json_inside.getString("pair2"));
                                product.setPair3(json_inside.getString("pair3"));
                                product.setPair4(json_inside.getString("pair4"));
                                product.setPair5(json_inside.getString("pair5"));
                                product.setPair6(json_inside.getString("pair6"));
                                product.setPair7(json_inside.getString("pair7"));
                                product.setPair8(json_inside.getString("pair8"));
                                product.setPair9(json_inside.getString("pair9"));
                                product.setPair10(json_inside.getString("pair10"));
                                product.setPair11(json_inside.getString("pair11"));
                                product.setPair12(json_inside.getString("pair12"));
                                product.setPairSpare(json_inside.getString("spare_Pin"));
                                product.setWiringAndBackPlate(json_inside.getString("wiringAndBackPlate"));
                                product.setInstructions(json_inside.getString("instructions"));
                                product.setApplicationDetails(json_inside.getString("applicationDetails"));
                                product.setIsCommissioningProduct(json_inside.getString("isCommissioningProduct"));
                                product.setVersion(json_inside.getInt("version"));
                                product.setIs_new_product(json_inside.getString("isNewInstall"));
                                product.setImage(json_inside.getString("image"));
                                product.setProductCategory(json_inside.getString("productCategory"));
                                product.setProductSubCategory(json_inside.getString("productSubCategory"));
                                product.setSearch_count(0);

                                mDb.productDetailsDao().InsertProduct(product);


                            }
                        }


                        dismissDialog();
                    }

                    int db_version = mDb.productDetailsDao().getVersion();

                    if (GlobalClass.isNetworkAvailable(context)) {
                        generateJwtToken(db_version, context);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }

    private static void generateJwtToken(int version, Context context) {

        showDialog("Checking version...");
        String tag_string_req = "req_login";
        StringRequest strReq = new StringRequest(Request.Method.POST, Helper.getConfigValue(ctx, "get_jwt_token"),response -> {

            try {
                JSONObject jObj = new JSONObject(response);
                String token = jObj.getString("token");
                getDataBaseVersion(token, version, context);

            } catch (Exception e) {
                e.printStackTrace();
                dismissDialog();
            }

        }, error -> dismissDialog()) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

        };
        addToRequestQueue(strReq, tag_string_req, context);
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private static void getDataBaseVersion(final String token, int version, Context context) {

        String tag_string_req = "req_login";
        StringRequest strReq = new StringRequest(Request.Method.GET, Helper.getConfigValue(ctx, "get_db_version"), response -> {

            try {

                JSONObject jObj = new JSONObject(response);
                int db_version = jObj.getInt("dbTableVersion");

                if (version != db_version) {
                    dismissDialog();
                    getDataFormDataBase(token, context);
                } else {
                    dismissDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
                dismissDialog();
            }

        }, error -> dismissDialog()) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        addToRequestQueue(strReq, tag_string_req, context);
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private static void getDataFormDataBase(String token, Context context) {
        showDialog2("Updating database...");
        String tag_string_req = "req_login";
        StringRequest strReq = new StringRequest(Request.Method.GET, Helper.getConfigValue(ctx, "get_installer_table_data"), response -> {
            try {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {

                        try {

                            JSONObject jObj = new JSONObject(response);
                            int responseCode = jObj.getInt("responseCode");

                            if (responseCode == GlobalClass.API_DATA_SUCCESS_CODE) {
                                progressDialog.dismiss();
                                Gson gson = new Gson();

                                DataBaseResponse responseObject = gson.fromJson(response, DataBaseResponse.class);
                                List<DataBaseModelResponse> product_list = responseObject.getResponse();

                                mDb.productDetailsDao().deleteAllData();

                                for (int i = 0; i < product_list.size(); i++) {

                                    ProductTableData product = new ProductTableData();

                                    product.setId(product_list.get(i).getId());
                                    product.setImage(product_list.get(i).getImage());
                                    product.setManufacturerName(product_list.get(i).getManufacturerName());
                                    product.setProductModelName(product_list.get(i).getProductModelName());
                                    product.setRecommendProductName(product_list.get(i).getRecommendProductName());
                                    product.setOtherCompatibleProductName(product_list.get(i).getOtherCompatibleProductName());

                                    product.setPair1(product_list.get(i).getPair1());
                                    product.setPair2(product_list.get(i).getPair2());
                                    product.setPair3(product_list.get(i).getPair3());
                                    product.setPair4(product_list.get(i).getPair4());
                                    product.setPair5(product_list.get(i).getPair5());
                                    product.setPair6(product_list.get(i).getPair6());
                                    product.setPair7(product_list.get(i).getPair7());
                                    product.setPair8(product_list.get(i).getPair8());
                                    product.setPair9(product_list.get(i).getPair9());
                                    product.setPair10(product_list.get(i).getPair10());
                                    product.setPair11(product_list.get(i).getPair11());
                                    product.setPair12(product_list.get(i).getPair12());
                                    product.setPairSpare(product_list.get(i).getPairSpare());

                                    product.setProductCategory(product_list.get(i).getProductCategory());
                                    product.setProductSubCategory(product_list.get(i).getProductSubCategory());

                                    product.setVersion(product_list.get(i).getVersion());
                                    product.setInstructions(product_list.get(i).getInstructions());
                                    product.setIs_new_product(product_list.get(i).getIsNewProduct());
                                    product.setApplicationDetails(product_list.get(i).getApplicationDetails());
                                    product.setWiringAndBackPlate(product_list.get(i).getWiringAndBackPlate());
                                    product.setIsCommissioningProduct(product_list.get(i).getIsCommissioningProduct());

                                    mDb.productDetailsDao().InsertProduct(product);

                                }
                            }
                            dismissDialog2();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                dismissDialog();
            }
        }, error -> dismissDialog()) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=UTF-8");
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };
        addToRequestQueue(strReq, tag_string_req, context);
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private static void showDialog(String message) {
        act.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.show();
                progressDialog.setMessage(message);
            }
        });
    }

    private static void showDialog2(String message) {
        act.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog2.show();
                progressDialog2.setMessage(message);
            }
        });

    }

    private static void dismissDialog2() {
        act.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog2.dismiss();
            }
        });
    }

    private static void dismissDialog() {
        act.runOnUiThread(new Runnable() {
            public void run() {
                progressDialog.dismiss();
            }
        });
    }

    public static RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            synchronized (LOCK) { // declare a private static Object to use for mutex
                if (mRequestQueue == null) {  // have to do this inside the sync
                    DefaultHttpClient httpclient = new DefaultHttpClient();
                    CookieStore cookieStore = new BasicCookieStore();
                    httpclient.setCookieStore(cookieStore);
                    HttpStack httpStack = new HttpClientStack(httpclient);
                    mRequestQueue = Volley.newRequestQueue(context, httpStack);
                    return mRequestQueue;
                }
            }
            return mRequestQueue;
        } else {
            return mRequestQueue;
        }
    }

    static public <T> void addToRequestQueue(Request<T> req, String tag, Context context) {
        req.setTag(TextUtils.isEmpty(tag) ? "MainHomePage" : tag);
        getRequestQueue(context).add(req);
    }
}

