package com.secure.installer.securedatabase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.secure.installer.model.ProductTableData;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SupportFactory;


@Database(entities = {ProductTableData.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public static final Object LOCK = new Object();
    public static AppDatabase sInstance;


    public static AppDatabase getInstance(Context context) {

        char[] testBytes = {'t', 'e', 's', 't'};

        final byte[] passphrase = SQLiteDatabase.getBytes(testBytes);
        final SupportFactory factory = new SupportFactory(passphrase);

        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, GlobalClass.DATABASE_NAME).openHelperFactory(factory).allowMainThreadQueries().build();
                }
            }
        }
        return sInstance;
    }

    public abstract ProductDetailsDao productDetailsDao();
}