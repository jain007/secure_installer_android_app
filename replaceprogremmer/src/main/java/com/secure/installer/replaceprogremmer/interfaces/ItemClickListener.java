package com.secure.installer.replaceprogremmer.interfaces;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View view, int position);
}
