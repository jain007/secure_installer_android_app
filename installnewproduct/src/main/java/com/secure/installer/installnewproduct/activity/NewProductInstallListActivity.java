package com.secure.installer.installnewproduct.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.secure.installer.installnewproduct.R;
import com.secure.installer.installnewproduct.adpt.ProductInstallRecyclerViewAdapter;
import com.secure.installer.installnewproduct.interfaces.ItemClickListener;
import com.secure.installer.model.ProductTableData;
import com.secure.installer.securedatabase.AppDatabase;
import com.secure.installer.securedatabase.AppExecutors;
import com.secure.installer.securedatabase.GlobalClass;

import java.util.ArrayList;
import java.util.List;

public class NewProductInstallListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView header;
    EditText et_search;
    RelativeLayout nodataLayout;

    List<ProductTableData> product_list = new ArrayList<>();
    ProductInstallRecyclerViewAdapter adapter;

    AppDatabase mDb;
    String productCategory;
    String productSubCategory;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_product_install_list_activity);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        nodataLayout = findViewById(R.id.nodataLayout);
        recyclerView = findViewById(R.id.recyclerView);
        et_search = findViewById(R.id.et_search);
        header = findViewById(R.id.header);


        // setup toolbar
        Toolbar mToolbar = findViewById(R.id.toolbar);
        GlobalClass.setToolbar(this, mToolbar, getString(R.string.new_product_installation));

        productCategory = getIntent().getStringExtra("product_category");
        productSubCategory = getIntent().getStringExtra("name");

        header.setText("Select " + productSubCategory);
        mDb = AppDatabase.getInstance(NewProductInstallListActivity.this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setUpData();

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // filter your list from your input
                filter(s.toString().toLowerCase());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.home) {
            GlobalClass.navigateToHomePage(NewProductInstallListActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }


    void filter(String text) {
        List<ProductTableData> temp = new ArrayList();
        for (ProductTableData d : product_list) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.getProductModelName().toLowerCase().contains(text)) {
                temp.add(d);
            }
        }
        //update recyclerview
        adapter.updateList(temp);
    }


    void setUpData() {

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                try {

                    if (productSubCategory.toLowerCase().equalsIgnoreCase("TimeSwitches & programmers")) {

                        List<ProductTableData> programmer_list = mDb.productDetailsDao().getSubCategoryProduct(productCategory, "programmer");
                        List<ProductTableData> timeswitch_list = mDb.productDetailsDao().getSubCategoryProduct(productCategory, "timeswitch");

                        product_list.addAll(programmer_list);
                        product_list.addAll(timeswitch_list);

                    } else {
                        product_list = mDb.productDetailsDao().getSubCategoryProduct(productCategory, productSubCategory.toLowerCase());
                    }

                    if (product_list.size() == 0) {
                        nodataLayout.setVisibility(View.VISIBLE);
                    } else {
                        nodataLayout.setVisibility(View.GONE);
                    }


                    adapter = new ProductInstallRecyclerViewAdapter(NewProductInstallListActivity.this, product_list, new ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            Intent intent = new Intent(NewProductInstallListActivity.this, ProductDetailsActivity.class);
                            intent.putExtra("product_name", "" + product_list.get(position).getProductModelName());
                            intent.putExtra("isCommissioningProduct", "" + product_list.get(position).getIsCommissioningProduct());
                            startActivity(intent);

                        }
                    });
                    recyclerView.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }
}
